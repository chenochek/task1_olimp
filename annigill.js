function findLatestWeight(weights) {
    while(true) {
        let len = weights.length;
        if(len == 1){
            return weights[0];
        } else if (len == 0) {
            return 0;
        }
        weights.sort((a, b) => a - b);//для оптимизации операцию надо заменить на поиск максимумов
        let diff = weights.pop() - weights.pop();
        len = weights.length;
        if(diff == 0){
            continue;
        } else if(diff > weights[len - 1]) {
            weights.push(diff);
        } else if (diff < weights[0]) {
            weights.unshift(diff);
        } else if (diff > weights[len / 2]) {
            weights.splice(len/2, 0, diff);
        } else {
            weights.splice(len/2 - 1, 0, diff);
        }
    }
    return 0;
}

module.exports = findLatestWeight;